package cz.cvut.fel.a7b39wpa.scrum.test;

import static cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus.*;
import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import static cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority.*;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductServiceTest extends AbstractServiceTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductBacklogItemService pbiService;

    @Test
    public void testAddAndRetrieveProduct() {
        String prodName = "product";
        String prodDesc = "this is some king of description";

        Long id = userService.addUser("name", "surname", "username", "passw");
        User user = userService.get(id);
        Long mem1 = userService.addUser("name1", "surname1", "username1", "pw1");
        Long mem2 = userService.addUser("name2", "surname2", "username2", "pw2");
        Long mem3 = userService.addUser("name3", "surname3", "username3", "pw3");
        Long mem4 = userService.addUser("name4", "surname4", "username4", "pw4");
        List<Long> list = new ArrayList<>();
        list.add(mem1);
        list.add(mem2);
        list.add(mem3);
        list.add(mem4);
        Long prodid = productService.addProduct(prodName, id, prodDesc, list);
        Product product = productService.get(prodid);

        assertEquals(prodName, product.getName());
        assertEquals(user, product.getOwner());
        assertEquals(prodDesc, product.getDescription());

    }

    @Test
    public void testBacklogItemAdding() {
        Long id = userService.addUser("name", "surname", "username", "passw");
        Long mem1 = userService.addUser("name1", "surname1", "username1", "pw1");
        Long mem2 = userService.addUser("name2", "surname2", "username2", "pw2");
        Long mem3 = userService.addUser("name3", "surname3", "username3", "pw3");
        Long mem4 = userService.addUser("name4", "surname4", "username4", "pw4");
        List<Long> list = new ArrayList<>();
        list.add(mem1);
        list.add(mem2);
        list.add(mem3);
        list.add(mem4);
        Long prodid = productService.addProduct("product", id, "this is some king of description", list);
        Product product = productService.get(prodid);

        Long pbiid = pbiService.addProductBacklogItem(prodid, "pbi", "this is description of pbi", SMALL);
        assertEquals(1, product.getProductBacklogItems().size());

        ProductBacklogItem pbi = pbiService.get(pbiid);
        assertEquals(pbi.getProduct(), product);
    }

    @Test
    public void testRemainingBacklogItems() {
        Long id = userService.addUser("name", "surname", "username", "passw");
        Long mem1 = userService.addUser("name1", "surname1", "username1", "pw1");
        Long mem2 = userService.addUser("name2", "surname2", "username2", "pw2");
        Long mem3 = userService.addUser("name3", "surname3", "username3", "pw3");
        Long mem4 = userService.addUser("name4", "surname4", "username4", "pw4");
        List<Long> list = new ArrayList<>();
        list.add(mem1);
        list.add(mem2);
        list.add(mem3);
        list.add(mem4);
        
        Long prodid = productService.addProduct("product", id, "this is some king of description", list);
        Product product = productService.get(prodid);
        Long pbiid = pbiService.addProductBacklogItem(prodid, "pbi", "this is description of pbi", SMALL);
        Long pbi2id = pbiService.addProductBacklogItem(prodid, "pbi2", "this is description of pbi", MEDIUM);
        ProductBacklogItem pbi = pbiService.get(pbi2id);
        
        pbi.setStatus(FINISHED);

        assertEquals(1, productService.getRemainingBacklogItems(prodid).size());
    }
}
