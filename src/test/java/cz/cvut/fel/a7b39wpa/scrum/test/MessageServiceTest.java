package cz.cvut.fel.a7b39wpa.scrum.test;

import cz.cvut.fel.a7b39wpa.scrum.bo.Message.MessageType;
import static cz.cvut.fel.a7b39wpa.scrum.bo.Message.MessageType.*;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.service.MessageService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class MessageServiceTest extends AbstractServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @Test
    public void testMessageSending() {
        String msgText = "Toto je zprava";
        String msgSubject = "toto je subject";
        MessageType msgType = IMPORTANT;

        Long user1id = userService.addUser("name", "surname", "username", "passw");
        User sender = userService.get(user1id);

        Long user2id = userService.addUser("name1", "surname1", "username1", "passw1");
        User reciever = userService.get(user2id);

        messageService.sendMessage(user1id, user2id, msgText, msgSubject, msgType);

        assertEquals(1, sender.getSentMessages().size());
        assertEquals(1, reciever.getReceivedMessages().size());
        assertEquals(sender.getSentMessages(), reciever.getReceivedMessages());
    }

}
