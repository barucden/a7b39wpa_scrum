package cz.cvut.fel.a7b39wpa.scrum.test;

import cz.cvut.fel.a7b39wpa.scrum.config.PersistenceConfig;
import cz.cvut.fel.a7b39wpa.scrum.config.ServiceConfig;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersistenceConfig.class, ServiceConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional(transactionManager = "txManager")
public abstract class AbstractServiceTest {

}
