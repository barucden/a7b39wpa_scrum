package cz.cvut.fel.a7b39wpa.scrum.test;

import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import cz.cvut.fel.a7b39wpa.scrum.tools.hash.CombinedHashProvider;
import cz.cvut.fel.a7b39wpa.scrum.tools.hash.HashProvider;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class UserServiceTest extends AbstractServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("combinedHashProvider")
    private HashProvider hashProvider;

    @Test
    public void testAndRetrieveUser() {
        String name = "name";
        String surname = "surname";
        String username = "username";
        String pw = "passw";

        Long id = userService.addUser(name, surname, username, pw);
        User testuser = userService.get(id);

        assertEquals(name, testuser.getName());
        assertEquals(surname, testuser.getSurname());
        assertEquals(username, testuser.getUsername());
        assertEquals(hashProvider.hash(pw), testuser.getPassword());
    }

    @Test
    public void testHashPassword() {
        Long id = userService.addUser("alfons", "pero", "peroid", "password");
        User user = userService.get(id);
        HashProvider p = new CombinedHashProvider();
        String pwHash = p.hash("password");
        assertEquals(pwHash, user.getPassword());
    }

    @Test
    public void testUpdateUser() {
        String nameChanged = "newname";
        Long id = userService.addUser("name", "surname", "username", "passw");
        User u = userService.get(id);
        u.setName(nameChanged);
        assertEquals(nameChanged, u.getName());
    }
    

}
