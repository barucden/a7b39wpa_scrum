package cz.cvut.fel.a7b39wpa.scrum.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackages = {"cz.cvut.fel.a7b39wpa.scrum.service", "cz.cvut.fel.a7b39wpa.scrum.tools.hash"})
public class ServiceConfig {
}
