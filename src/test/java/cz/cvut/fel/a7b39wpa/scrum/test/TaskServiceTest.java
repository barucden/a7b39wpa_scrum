package cz.cvut.fel.a7b39wpa.scrum.test;

import static cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus.TODO;
import static cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority.*;
import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.Task;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintService;
import cz.cvut.fel.a7b39wpa.scrum.service.TaskService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TaskServiceTest extends AbstractServiceTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private SprintBacklogItemService sbiService;

    @Autowired
    private ProductBacklogItemService pbiService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SprintService sprintService;

    @Test
    public void testAddAndRetrieveTask() {
        Date startDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(Calendar.DATE, 1);
        Date endDate = c.getTime();

        Long id = userService.addUser("name", "surname", "username", "passw");
        User user = userService.get(id);
        Long mem1 = userService.addUser("name1", "surname1", "username1", "pw1");
        Long mem2 = userService.addUser("name2", "surname2", "username2", "pw2");
        Long mem3 = userService.addUser("name3", "surname3", "username3", "pw3");
        Long mem4 = userService.addUser("name4", "surname4", "username4", "pw4");
        List<Long> list = new ArrayList<>();
        list.add(mem1);
        list.add(mem2);
        list.add(mem3);
        list.add(mem4);
        Long prodid = productService.addProduct("product", id, "this is some king of description", list);
        Long pbiid = pbiService.addProductBacklogItem(prodid, "pbi", "this is description of pbi", SMALL);
        Long sprintid = sprintService.addSprint(startDate, endDate, "sprint", "sprint description", prodid);
        Long sbiid = sbiService.addSprintBacklogItem(sprintid, pbiid, "sbiname", "description of SBI", TODO);
        SprintBacklogItem sbi = sbiService.get(sbiid);
        
        String taskName = "taskName";
        String taskDesc = "this is description of pbi";

        Long taskid = taskService.addTask(sbiid, taskName, taskDesc, id);
        Task task = taskService.get(taskid);

        assertEquals(sbi, task.getItem());
        assertEquals(taskName, task.getName());
        assertEquals(taskDesc, task.getDescription());
        assertEquals(user, task.getAuthor());
    }

}
