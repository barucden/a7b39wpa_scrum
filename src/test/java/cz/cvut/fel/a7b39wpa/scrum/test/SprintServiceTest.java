package cz.cvut.fel.a7b39wpa.scrum.test;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem;
import static cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus.TODO;
import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import static cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority.*;
import cz.cvut.fel.a7b39wpa.scrum.bo.Sprint;
import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class SprintServiceTest extends AbstractServiceTest {

    @Autowired
    private SprintService sprintService;

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductBacklogItemService pbiService;

    @Autowired
    private SprintBacklogItemService sbiService;

    @Test
    public void testAddAndRetrieveSprint() {
        String prodName = "product";
        String prodDesc = "this is some king of description";
        String sprintName = "sprint";
        String sprintDesc = "sprint description";
        Date startDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(Calendar.DATE, 10);
        Date endDate = c.getTime();

        Long id = userService.addUser("name", "surname", "username", "passw");
        Long mem1 = userService.addUser("name1", "surname1", "username1", "pw1");
        Long mem2 = userService.addUser("name2", "surname2", "username2", "pw2");
        Long mem3 = userService.addUser("name3", "surname3", "username3", "pw3");
        Long mem4 = userService.addUser("name4", "surname4", "username4", "pw4");
        List<Long> list = new ArrayList<>();
        list.add(mem1);
        list.add(mem2);
        list.add(mem3);
        list.add(mem4);
        Long prodid = productService.addProduct(prodName, id, prodDesc, list);
        Product product = productService.get(prodid);
        Long sprintid = sprintService.addSprint(startDate, endDate, sprintName, sprintDesc, prodid);
        Sprint sprint = sprintService.get(sprintid);
        SprintDTO sprintdto = sprintService.getAsDTO(sprintid);

        assertEquals(startDate, sprint.getStartDate());
        assertEquals(9, sprintService.daysLeft(sprintdto));
        assertEquals(sprintName, sprint.getName());
        assertEquals(sprintDesc, sprint.getDescription());
        assertEquals(product, sprint.getProduct());
    }


    @Test
    public void testSprintDaysLeft() {       
        Long id = userService.addUser("name", "surname", "username", "passw");
        Long mem1 = userService.addUser("name1", "surname1", "username1", "pw1");
        Long mem2 = userService.addUser("name2", "surname2", "username2", "pw2");
        Long mem3 = userService.addUser("name3", "surname3", "username3", "pw3");
        Long mem4 = userService.addUser("name4", "surname4", "username4", "pw4");
        List<Long> list = new ArrayList<>();
        list.add(mem1);
        list.add(mem2);
        list.add(mem3);
        list.add(mem4);
        
        Long prodid = productService.addProduct("product", id, "this is some king of description", list);
        
        Date startDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(Calendar.DATE, 10);
        Date endDate = c.getTime();
        
        Long sprintid = sprintService.addSprint(startDate, endDate, "sprint", "sprint description", prodid);
        SprintDTO sprint = sprintService.getAsDTO(sprintid);

        assertEquals(9, sprintService.daysLeft(sprint));
    }

}
