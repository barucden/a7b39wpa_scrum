package cz.cvut.fel.a7b39wpa.scrum.test;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus;
import static cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus.*;
import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority;
import static cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority.*;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductBacklogItemServiceTest extends AbstractServiceTest {

    @Autowired
    private ProductBacklogItemService pbiService;

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Test
    public void testAddProductBacklogItem() {
        Long ownerId = userService.addUser("name", "surname", "username", "passw");
        Long mem1 = userService.addUser("name1", "surname1", "username1", "pw1");
        Long mem2 = userService.addUser("name2", "surname2", "username2", "pw2");
        Long mem3 = userService.addUser("name3", "surname3", "username3", "pw3");
        Long mem4 = userService.addUser("name4", "surname4", "username4", "pw4");
        List<Long> list = new ArrayList<>();
        list.add(mem1);
        list.add(mem2);
        list.add(mem3);
        list.add(mem4);

        Long prodId = productService.addProduct("product", ownerId, "description of a product", list);
        Product product = productService.get(prodId);

        String pbiName = "pbi";
        String pbiDesc = "this is description of pbi";
        Priority priority = MEDIUM;
        ItemStatus status = TODO;

        Long pbiId = pbiService.addProductBacklogItem(prodId, pbiName, pbiDesc, priority);
        ProductBacklogItem pbi = pbiService.get(pbiId);

        assertEquals(product, pbi.getProduct());
        assertEquals(pbiName, pbi.getName());
        assertEquals(pbiDesc, pbi.getDescription());
        assertEquals(priority, pbi.getPriority());
        assertEquals(status, pbi.getStatus());

    }

}
