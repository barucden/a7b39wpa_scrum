package cz.cvut.fel.a7b39wpa.scrum.dto;

import java.util.List;

public class ProductDTO extends AbstractDTO {

    private String name;
    private Long owner;
    private String description;
    private List<Long> productBacklogItems;
    private List<Long> sprints;

    public ProductDTO(Long id, String name, Long owner, String description, List<Long> productBacklogItems, List<Long> sprints) {
        super(id);
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.productBacklogItems = productBacklogItems;
        this.sprints = sprints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getProductBacklogItems() {
        return productBacklogItems;
    }

    public void setProductBacklogItems(List<Long> productBacklogItems) {
        this.productBacklogItems = productBacklogItems;
    }

    public List<Long> getSprints() {
        return sprints;
    }

    public void setSprints(List<Long> sprints) {
        this.sprints = sprints;
    }

}
