package cz.cvut.fel.a7b39wpa.scrum.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;

@Entity
public class Sprint extends AbstractEntity implements Serializable {

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    private String name;

    private String description;

    private Boolean closed;

    @ManyToOne
    private Product product;

    @OneToMany(mappedBy = "sprint")
    @OrderBy("name")
    private List<SprintBacklogItem> sprintBacklogItems;

    protected Sprint() {
    }

    public Sprint(Date startDate, Date endDate, String name, String description, Product product, Boolean closed) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.name = name;
        this.description = description;
        this.product = product;
        this.closed = closed;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public List<SprintBacklogItem> getSprintBacklogItems() {
        if (sprintBacklogItems == null) {
            sprintBacklogItems = new ArrayList<>();
        }
        return sprintBacklogItems;
    }

    public void setSprintBacklogItems(List<SprintBacklogItem> sprintBacklogItems) {
        this.sprintBacklogItems = sprintBacklogItems;
    }

    public void addSprintBacklogItem(SprintBacklogItem sprintBacklogItem) {
        if (sprintBacklogItems == null) {
            sprintBacklogItems = new ArrayList<>();
        }
        this.sprintBacklogItems.add(sprintBacklogItem);
    }

}
