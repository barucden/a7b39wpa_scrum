package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.EntityIDFetcher;

public class ProductDTOTransformer implements DTOTransformer<Product, ProductDTO> {

    @Override
    public ProductDTO getDTO(Product p) {
        return new ProductDTO(p.getId(), p.getName(), p.getOwner().getId(), p.getDescription(), EntityIDFetcher.getIDs(p.getProductBacklogItems()), EntityIDFetcher.getIDs(p.getSprints()));
    }

}
