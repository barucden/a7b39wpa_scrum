package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.Task;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.SprintBacklogItemDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.TaskDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.UserDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TaskService extends AbstractService<Task, Long> {

    @Autowired
    private TaskDAO primaryDAO;
    @Autowired
    private SprintBacklogItemDAO sprintBacklogItemDAO;
    @Autowired
    private UserDAO userDAO;

    @Override
    protected GenericDAO<Task, Long> getPrimaryDAO() {
        return primaryDAO;
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToSeeSbi(#itemId)")
    public Long addTask(Long itemId, String name, String description, Long authorId) {
        SprintBacklogItem item = sprintBacklogItemDAO.find(itemId);
        User author = userDAO.find(authorId);
        Task task = new Task(item, name, description, author);
        persist(task);
        item.addTask(task);
        sprintBacklogItemDAO.update(item);
        return task.getId();
    }

    @Transactional(readOnly = true)
    @PreAuthorize("@permissionService.hasPermissionToSeeSbi(#id)")
    public List<Task> getTasksBySprintBacklogItem(Long id) {
        SprintBacklogItem item = sprintBacklogItemDAO.find(id);
        return item.getTasks();
    }

}
