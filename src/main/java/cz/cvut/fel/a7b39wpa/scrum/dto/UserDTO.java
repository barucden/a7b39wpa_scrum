package cz.cvut.fel.a7b39wpa.scrum.dto;

import java.util.Date;
import java.util.List;

public class UserDTO extends AbstractDTO {

    private String name;
    private String surname;
    private String username;
    private Date dateRegistered;
    private List<Long> sentMessages;
    private List<Long> receivedMessages;

    public UserDTO(Long id, String name, String surname, String username, Date dateRegistered, List<Long> sentMessages, List<Long> receivedMessages) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.dateRegistered = dateRegistered;
        this.sentMessages = sentMessages;
        this.receivedMessages = receivedMessages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(Date dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    public List<Long> getSentMessages() {
        return sentMessages;
    }

    public void setSentMessages(List<Long> sentMessages) {
        this.sentMessages = sentMessages;
    }

    public List<Long> getReceivedMessages() {
        return receivedMessages;
    }

    public void setReceivedMessages(List<Long> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }
    
    @Override
    public String toString() {
        return surname + " " + name + " [" + username + "]";
    }        

}
