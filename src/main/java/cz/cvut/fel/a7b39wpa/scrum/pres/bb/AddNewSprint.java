package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintService;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import org.primefaces.event.FlowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session")
public class AddNewSprint {

    @Autowired
    private ProductService productService;
    @Autowired
    private SprintService sprintService;
    @Autowired
    private SprintBacklogItemService sprintBacklogItemService;

    private ProductDTO product;

    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private ProductBacklogItemsPickList productBacklogItemsPickList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ProductBacklogItemsPickList getProductBacklogItemsPickList() {
        if (productBacklogItemsPickList == null) {
            productBacklogItemsPickList = new ProductBacklogItemsPickList(productService.getRemainingBacklogItems(product.getId()));
        }
        return productBacklogItemsPickList;
    }

    public void setProductBacklogItemsPickList(ProductBacklogItemsPickList productBacklogItemsPickList) {
        this.productBacklogItemsPickList = productBacklogItemsPickList;
    }

    public String onFlowProcess(FlowEvent e) {
        return e.getNewStep();
    }

    public String setProductById(String outcome) {
        Long productId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productId"));
        this.product = productService.getAsDTO(productId);
        return outcome;
    }

    public String submit() {
        Long sprintId = sprintService.addSprint(startDate, endDate, name, description, product.getId());
        List<ProductBacklogItemDTO> productItems = productBacklogItemsPickList.getItems().getTarget();
        for (ProductBacklogItemDTO item : productItems) {
            sprintBacklogItemService.addSprintBacklogItem(sprintId, item.getId(), item.getName(), item.getDescription(), AbstractBacklogItem.ItemStatus.TODO);
        }
        name = null;
        description = null;
        startDate = null;
        endDate = null;
        productBacklogItemsPickList = null;
        return "productDetail?faces-redirect=true";
    }

    public ProductDTO getProduct() {
        return product;
    }

}
