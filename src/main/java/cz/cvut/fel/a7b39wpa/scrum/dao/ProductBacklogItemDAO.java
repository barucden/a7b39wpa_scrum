package cz.cvut.fel.a7b39wpa.scrum.dao;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus;
import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

@Component
public class ProductBacklogItemDAO extends BaseDAO<ProductBacklogItem, Long> {

    public ProductBacklogItemDAO() {
        super(ProductBacklogItem.class);
    }

    public void setState(Long pbiId, ItemStatus state) {
        ProductBacklogItem pbi = find(pbiId);
        pbi.setStatus(state);
        update(pbi);
    }
    
    public List<ProductBacklogItem> getRemainingBacklogItemsByProduct(Long id) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductBacklogItem> cq = cb.createQuery(ProductBacklogItem.class);
        Root<ProductBacklogItem> pbi = cq.from(ProductBacklogItem.class);
        Join<ProductBacklogItem, Product> product = pbi.join("product", JoinType.INNER);
        cq.select(pbi);
        ParameterExpression<Long> pId = cb.parameter(Long.class);
        ParameterExpression<ItemStatus> pStatus = cb.parameter(ItemStatus.class);
        cq.where(
                cb.and(
                        cb.equal(product.get("id"), pId),
                        cb.notEqual(pbi.get("status"), pStatus)
                )
        );
        cq.orderBy(
                cb.desc(pbi.get("priority")), cb.desc(pbi.get("name"))
        );
        TypedQuery<ProductBacklogItem> query = entityManager.createQuery(cq);
        query.setParameter(pId, id);
        query.setParameter(pStatus, ItemStatus.FINISHED);
        return query.getResultList();
    }

    public List<ProductBacklogItem> getProductBacklogItemsByProduct(Long id) {
        return entityManager.createQuery("SELECT e FROM " + type.getName() + " e WHERE e.product.id = ?1", type).setParameter(1, id).getResultList();
    }
}
