package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.Message.MessageType;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dto.UserDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.MessageService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request")
public class ComposeNewMessage {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;

    private String receiver;
    private String subject;
    private String text;
    private MessageType type;

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public MessageType[] getTypes() {
        return messageService.getMessageTypes();
    }

    public String send() {
        UserDTO sender = userService.getCurrentUser();
        User receiver = userService.getUserByUsername(this.receiver);
        messageService.sendMessage(sender.getId(), receiver.getId(), subject, text, type);
        return "composeNewMessage?faces-redirect=true&success";
    }

}
