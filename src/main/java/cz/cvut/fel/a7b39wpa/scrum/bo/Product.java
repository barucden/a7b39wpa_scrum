package cz.cvut.fel.a7b39wpa.scrum.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class Product extends AbstractEntity implements Serializable{

    @Column(nullable = false, unique = true)
    private String name;

    @JoinColumn(nullable = false)
    @ManyToOne
    private User owner;

    @Column(nullable = false)
    private String description;
    
    @ManyToMany
    private List<User> members;

    @OneToMany(mappedBy = "product")
    @OrderBy("priority DESC, status DESC, name")
    private List<ProductBacklogItem> productBacklogItems;

    @OneToMany(mappedBy = "product")
    private List<Sprint> sprints;

    protected Product() {
    }

    public Product(String name, User owner, String description, List<User> members) {
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProductBacklogItem> getProductBacklogItems() {
        if (productBacklogItems == null) {
            productBacklogItems = new ArrayList<>();
        }
        return productBacklogItems;
    }

    public void setProductBacklogItems(List<ProductBacklogItem> productBacklogItems) {
        this.productBacklogItems = productBacklogItems;
    }

    public void addProductBacklogItem(ProductBacklogItem item) {
        if (productBacklogItems == null) {
            productBacklogItems = new ArrayList<>();
        }
        productBacklogItems.add(item);
    }

    public List<Sprint> getSprints() {
        if (sprints == null) {
            sprints = new ArrayList<>();
        }
        return sprints;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }

    public void addSprint(Sprint sprint) {
        if (sprints == null) {
            sprints = new ArrayList<>();
        }
        this.sprints.add(sprint);
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public void addMember(User u) {
        if (members == null) {
            members = new ArrayList<>();
        }
        members.add(u);
    }
    
}
