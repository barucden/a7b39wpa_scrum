package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.bo.Sprint;
import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class PermissionService {

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    @Autowired
    SprintService sprintService;

    @Autowired
    SprintBacklogItemService sprintBacklogItemService;

    public Boolean hasPermissionToManageUsers() {
        for (GrantedAuthority a : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if (a.getAuthority().equals("ROLE_ADMIN")) {
                return true;
            }
        }
        return false;
    }

    public Boolean hasPermissionToSeeProduct(Long productId) {
        Product p = productService.get(productId);
        UserDTO loggedUser = userService.getCurrentUser();
        for (User u : p.getMembers()) {
            if (u.getId().equals(loggedUser.getId())) {
                return true;
            }
        }
        return false;
    }

    public Boolean hasPermissionToAddPbiToProduct(Long productId) {
        return hasPermissionToSeeProduct(productId);
    }

    public Boolean hasPermissionToSeeUsersMessages(Long userId) {
        return userService.getCurrentUser().getId().equals(userId);
    }

    public Boolean hasPermissionToSeeSprint(Long sprintId) {
        Sprint sprint = sprintService.get(sprintId);
        return hasPermissionToSeeProduct(sprint.getProduct().getId());
    }

    public Boolean hasPermissionToSeeSbi(Long sbiId) {
        SprintBacklogItem sbi = sprintBacklogItemService.get(sbiId);
        return hasPermissionToSeeSprint(sbi.getSprint().getId());
    }
}
