package cz.cvut.fel.a7b39wpa.scrum.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"sprint_id", "name"})
)
@Entity
public class SprintBacklogItem extends AbstractBacklogItem implements Serializable{

    @ManyToOne
    @JoinColumn(nullable = false, name = "sprint_id")
    private Sprint sprint;

    @ManyToOne
    private ProductBacklogItem productBacklogItem;

    @OneToMany(mappedBy = "item")
    private List<Task> tasks;

    protected SprintBacklogItem() {

    }

    public SprintBacklogItem(Sprint sprint, ProductBacklogItem productBacklogItem, String name, String description, ItemStatus status) {
        super(name, description, status);
        this.sprint = sprint;
        this.productBacklogItem = productBacklogItem;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public ProductBacklogItem getProductBacklogItem() {
        return productBacklogItem;
    }

    public void setProductBacklogItem(ProductBacklogItem productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    public List<Task> getTasks() {
        if (tasks == null) {
            tasks = new ArrayList<>();
        }
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task) {
        if (tasks == null) {
            tasks = new ArrayList<>();
        }
        this.tasks.add(task);
    }

}
