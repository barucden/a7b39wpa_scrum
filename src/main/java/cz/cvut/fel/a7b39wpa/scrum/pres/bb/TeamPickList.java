package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.model.DualListModel;

public class TeamPickList {

    private DualListModel<User> users;

    public TeamPickList(List<User> users) {
        this.users = new DualListModel<>(users, new ArrayList<>());

    }

    public DualListModel<User> getUsers() {
        return users;
    }

    public void setUsers(DualListModel<User> users) {
        this.users = users;
    }

}
