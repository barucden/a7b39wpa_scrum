package cz.cvut.fel.a7b39wpa.scrum.dao;

import cz.cvut.fel.a7b39wpa.scrum.bo.Task;
import org.springframework.stereotype.Component;

@Component
public class TaskDAO extends BaseDAO<Task, Long> {

    public TaskDAO() {
        super(Task.class);
    }

}
