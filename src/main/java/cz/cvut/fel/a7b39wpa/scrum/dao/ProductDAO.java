package cz.cvut.fel.a7b39wpa.scrum.dao;

import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ProductDAO extends BaseDAO<Product, Long> {

    public ProductDAO() {
        super(Product.class);
    }

    public Product getByName(String name) {
        List<Product> list = entityManager.createQuery("SELECT e FROM " + type.getName() + " e WHERE e.name = ?1", type).setParameter(1, name).getResultList();
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

}
