package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.UserDAO;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dto.UserDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.hash.HashProvider;
import cz.cvut.fel.a7b39wpa.scrum.tools.trans.UserDTOTransformer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService extends AbstractService<User, Long> {

    @Autowired
    private UserDAO primaryDAO;

    @Override
    protected GenericDAO<User, Long> getPrimaryDAO() {
        return primaryDAO;
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToManageUsers()")
    public Long addUser(String name, String surname, String username, String password) {
        User user = new User(name, surname, username, password, new Date());
        getPrimaryDAO().persist(user);
        return user.getId();
    }

    @Transactional
    public User getUserByUsername(String username) {
        return primaryDAO.findByUsername(username);
    }

    public UserDTO getCurrentUser() {
        return new UserDTOTransformer().getDTO(getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()));
    }

    @Transactional(readOnly = true)
    public List<User> getUsersFromDTOs(List<UserDTO> dtos) {
        List<User> users = new ArrayList<>(dtos.size());
        for (UserDTO dto : dtos) {
            User u = get(dto.getId());
            if (u != null) {
                users.add(u);
            }
        }
        return users;
    }

    @Override
    @PreAuthorize("@permissionService.hasPermissionToManageUsers()")
    public void remove(User instance) {
        super.remove(instance);
    }

    public List<UserDTO> getAllAsDTO() {
        return new UserDTOTransformer().getDTOList(getAll());
    }

}
