package cz.cvut.fel.a7b39wpa.scrum.dto;

import java.util.List;
import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem;

public class SprintBacklogItemDTO extends AbstractBacklogItemDTO {

    private Long sprint;
    private Long productBacklogItem;
    private List<Long> tasks;

    public SprintBacklogItemDTO(Long id, String name, String description, AbstractBacklogItem.ItemStatus status, Long sprint, Long productBacklogItem, List<Long> tasks) {
        super(id, name, description, status);
        this.sprint = sprint;
        this.productBacklogItem = productBacklogItem;
        this.tasks = tasks;
    }

    public Long getSprint() {
        return sprint;
    }

    public void setSprint(Long sprint) {
        this.sprint = sprint;
    }

    public Long getProductBacklogItem() {
        return productBacklogItem;
    }

    public void setProductBacklogItem(Long productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    public List<Long> getTasks() {
        return tasks;
    }

    public void setTasks(List<Long> tasks) {
        this.tasks = tasks;
    }

}
