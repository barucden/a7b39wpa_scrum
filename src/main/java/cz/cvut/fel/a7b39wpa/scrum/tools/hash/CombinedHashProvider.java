package cz.cvut.fel.a7b39wpa.scrum.tools.hash;

import org.springframework.stereotype.Component;

@Component("combinedHashProvider")
public class CombinedHashProvider implements HashProvider {

    private static final String SALT = "$1L%^9H2ho1}:bd;al3sx2x49y";

    @Override
    public String hash(String s) {
        s = s.concat(SALT);
        HashProvider sha1 = new SHA1HashProvider();
        HashProvider md5 = new MD5HashProvider();
        return sha1.hash(md5.hash(s).concat(s));
    }

}
