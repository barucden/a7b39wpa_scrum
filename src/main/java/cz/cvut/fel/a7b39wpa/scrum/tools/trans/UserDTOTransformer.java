package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dto.UserDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.EntityIDFetcher;
import java.util.ArrayList;
import java.util.List;

public class UserDTOTransformer implements DTOTransformer<User, UserDTO>, DTOMultiTransformer<User, UserDTO> {

    @Override
    public UserDTO getDTO(User u) {
        return new UserDTO(u.getId(), u.getName(), u.getSurname(), u.getUsername(), u.getRegistered(), EntityIDFetcher.getIDs(u.getSentMessages()), EntityIDFetcher.getIDs(u.getReceivedMessages()));
    }

    @Override
    public List<UserDTO> getDTOList(List<User> l) {
        List<UserDTO> dtos = new ArrayList<>(l.size());
        for (User u : l) {
            if (u != null) {
                dtos.add(getDTO(u));
            }
        }
        return dtos;
    }

}
