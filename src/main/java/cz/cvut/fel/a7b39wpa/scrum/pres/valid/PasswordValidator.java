package cz.cvut.fel.a7b39wpa.scrum.pres.valid;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;

@Component
public class PasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        UIInput firstInput = (UIInput) component.getAttributes().get("password");
        String first = (String) firstInput.getValue();

        if (!first.equals((String) value)) {
            throw new ValidatorException(new FacesMessage("Passwords must match."));
        }
    }

}
