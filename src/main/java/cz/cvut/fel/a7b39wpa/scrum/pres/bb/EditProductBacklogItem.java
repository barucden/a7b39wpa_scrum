package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session")
public class EditProductBacklogItem {

    @Autowired
    private ProductBacklogItemService productBacklogItemService;

    private ProductBacklogItem productBacklogItem;   
    
    public ProductBacklogItem getProductBacklogItem() {
        return productBacklogItem;
    }
    
    public Priority[] getPriorities() {
        return Priority.values();
    }
    
    public ItemStatus[] getStatuses() {
        return ItemStatus.values();
    }

    public String submit(String outcome) {
        productBacklogItemService.update(productBacklogItem);
        FacesMessage msg = new FacesMessage("ProductBacklogItem updated.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return outcome;
    }
    
    public void setProductBacklogItem(ProductBacklogItem productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    public String setProductBacklogItemById(String outcome) {
        Long productId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productBacklogItemId"));
        productBacklogItem = productBacklogItemService.get(productId);
        return outcome;
    }

}
