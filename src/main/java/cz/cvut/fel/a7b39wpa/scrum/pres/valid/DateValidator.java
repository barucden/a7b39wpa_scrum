package cz.cvut.fel.a7b39wpa.scrum.pres.valid;

import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.stereotype.Component;

@Component
public class DateValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {        
        UIInput startDateInput = (UIInput) component.getAttributes().get("maskcalendarstart");

        Date startDate = (Date) startDateInput.getValue();
        Date endDate = (Date) value;
        if (endDate.before(startDate)) {
            throw new ValidatorException(new FacesMessage("The end date must be after the start date."));
        }
    }

}
