package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductBacklogItemDTO;
import java.util.ArrayList;
import java.util.List;

public class ProductBacklogItemDTOTransformer implements DTOTransformer<ProductBacklogItem, ProductBacklogItemDTO>, DTOMultiTransformer<ProductBacklogItem, ProductBacklogItemDTO> {

    @Override
    public ProductBacklogItemDTO getDTO(ProductBacklogItem t) {
        return new ProductBacklogItemDTO(t.getId(), t.getName(), t.getDescription(), t.getStatus(), t.getProduct().getId(), t.getPriority());
    }

    @Override
    public List<ProductBacklogItemDTO> getDTOList(List<ProductBacklogItem> l) {
        List<ProductBacklogItemDTO> dtos = new ArrayList<>(l.size());
        for (ProductBacklogItem item : l) {
            if (item != null) {
                dtos.add(getDTO(item));
            }
        }
        return dtos;
    }

}
