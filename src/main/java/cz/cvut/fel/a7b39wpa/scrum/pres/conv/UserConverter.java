package cz.cvut.fel.a7b39wpa.scrum.pres.conv;

import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements Converter{

    @Autowired
    UserService userService;
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        int firstBracketIndex = string.indexOf("[");
        int secondBracketIndex = string.indexOf("]");
        String username = string.substring(firstBracketIndex + 1, secondBracketIndex);
        User u = userService.getUserByUsername(username);
        return u;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        User u = (User)o;
        return u.getName() + " " + u.getSurname() + " [" + u.getUsername() + "]";
    }
    
}
