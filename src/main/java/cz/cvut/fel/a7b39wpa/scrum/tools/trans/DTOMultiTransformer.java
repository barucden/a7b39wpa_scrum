package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractEntity;
import cz.cvut.fel.a7b39wpa.scrum.dto.AbstractDTO;
import java.util.List;

public interface DTOMultiTransformer<T1 extends AbstractEntity, T2 extends AbstractDTO> {
    List<T2> getDTOList(List<T1> l);
}
