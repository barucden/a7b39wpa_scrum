package cz.cvut.fel.a7b39wpa.scrum.dto;

import java.util.Date;
import cz.cvut.fel.a7b39wpa.scrum.bo.Message;
import cz.cvut.fel.a7b39wpa.scrum.bo.Message.MessageType;

public class MessageDTO extends AbstractDTO {

    private Long sender;
    private Long receiver;
    private MessageType tp;
    private String text;
    private Date timeSent;
    private Boolean isRead;

    public MessageDTO(Long id, Long sender, Long receiver, MessageType tp, String text, Date timeSent, Boolean isRead) {
        super(id);
        this.sender = sender;
        this.receiver = receiver;
        this.tp = tp;
        this.text = text;
        this.timeSent = timeSent;
        this.isRead = isRead;
    }

    public Long getSender() {
        return sender;
    }

    public void setSender(Long sender) {
        this.sender = sender;
    }

    public Long getReceiver() {
        return receiver;
    }

    public void setReceiver(Long receiver) {
        this.receiver = receiver;
    }

    public Message.MessageType getTp() {
        return tp;
    }

    public void setTp(Message.MessageType tp) {
        this.tp = tp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

}
