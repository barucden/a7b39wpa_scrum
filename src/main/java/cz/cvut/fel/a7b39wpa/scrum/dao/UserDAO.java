package cz.cvut.fel.a7b39wpa.scrum.dao;

import cz.cvut.fel.a7b39wpa.scrum.bo.Message;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UserDAO extends BaseDAO<User, Long> {

    public UserDAO() {
        super(User.class);
    }

    public User findByUsername(String username) {
        List<User> list = entityManager.createQuery("SELECT u FROM " + type.getName() + " u WHERE u.username = ?1", type).setParameter(1, username).getResultList();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public List<Message> getReceivedMesseges(Long id) {
        return loadById(id).getReceivedMessages();
    }

    public List<Message> getSentMesseges(Long id) {
        return loadById(id).getSentMessages();
    }

    public List<User> getUsersByID(List<Long> userIds) {
        List<User> users = new ArrayList<>(userIds.size());
        for (Long id : userIds) {
            users.add(find(id));
        }
        return users;
    }

}
