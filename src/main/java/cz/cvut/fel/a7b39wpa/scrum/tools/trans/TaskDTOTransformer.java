package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.Task;
import cz.cvut.fel.a7b39wpa.scrum.dto.TaskDTO;
import java.util.List;

public class TaskDTOTransformer implements DTOTransformer<Task, TaskDTO>, DTOMultiTransformer<Task, TaskDTO> {

    @Override
    public TaskDTO getDTO(Task t) {
        return new TaskDTO(t.getId(), t.getItem().getId(), t.getName(), t.getDescription(), t.getAuthor().getId());
    }

    @Override
    public List<TaskDTO> getDTOList(List<Task> l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
