package cz.cvut.fel.a7b39wpa.scrum.bo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"product_id", "name"})
)
@Entity
public class ProductBacklogItem extends AbstractBacklogItem implements Serializable{

    public enum Priority {
        LOW(0),
        SMALL(1),
        MEDIUM(2),
        HIGH(3),
        VERY_HIGH(4),
        CRITICAL(5);

        private final int value;

        Priority(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    @ManyToOne
    @JoinColumn(nullable = false, name = "product_id")
    private Product product;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Priority priority;

    protected ProductBacklogItem() {
    }

    public ProductBacklogItem(Product product, String name, String description, Priority priority, ItemStatus status) {
        super(name, description, status);
        this.product = product;
        this.priority = priority;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

}
