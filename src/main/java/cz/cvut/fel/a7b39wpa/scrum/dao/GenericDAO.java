package cz.cvut.fel.a7b39wpa.scrum.dao;

import java.util.List;

public interface GenericDAO<T, ID> {

    public List<T> findAll();

    public T find(ID id);

    public Long getCount();

    public void remove(T instance);

    public void persist(T instance);

    public void update(T instance);

    public T loadById(ID id);

}
