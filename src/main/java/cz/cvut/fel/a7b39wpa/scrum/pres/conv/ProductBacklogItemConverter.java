package cz.cvut.fel.a7b39wpa.scrum.pres.conv;

import cz.cvut.fel.a7b39wpa.scrum.dto.ProductBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductBacklogItemConverter implements Converter {

    @Autowired
    private ProductBacklogItemService backlogItemService;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return backlogItemService.getAsDto(Long.parseLong(string));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((ProductBacklogItemDTO) o).getId().toString();
    }

}
