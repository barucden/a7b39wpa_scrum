package cz.cvut.fel.a7b39wpa.scrum.bo;

import cz.cvut.fel.a7b39wpa.scrum.tools.hash.HashProvider;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

@Configurable(preConstruction = true)
@Entity
@Table(name = "users")
public class User extends AbstractEntity implements Serializable {

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date registered;

    @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY)
    @OrderBy("isRead, timeSent")
    private List<Message> receivedMessages;

    @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY)
    @OrderBy("timeSent")
    private List<Message> sentMessages;

    @Autowired
    @Qualifier("combinedHashProvider")
    private transient HashProvider hashProvider;

    @ManyToMany(mappedBy = "members")    
    private List<Product> products;

    protected User() {
    }

    public User(String name, String surname, String username, String password, Date registered) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.registered = registered;
        setPassword(password);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public Boolean hasPassword(String password) {
        System.out.println(hashProvider);        
        return hashProvider.hash(password).equals(getPassword());
    }

    public void setPassword(String password) {
        this.password = hashProvider.hash(password);
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public List<Message> getReceivedMessages() {
        if (receivedMessages == null) {
            receivedMessages = new ArrayList<>();
        }
        return receivedMessages;
    }

    public void setReceivedMessages(List<Message> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }

    public void addReceivedMessage(Message message) {
        if (receivedMessages == null) {
            receivedMessages = new ArrayList<>();
        }
        this.receivedMessages.add(message);
    }

    public List<Message> getSentMessages() {
        if (sentMessages == null) {
            sentMessages = new ArrayList<>();
        }
        return sentMessages;
    }

    public void setSentMessages(List<Message> sentMessages) {
        this.sentMessages = sentMessages;
    }

    public void addSentMessage(Message message) {
        if (sentMessages == null) {
            sentMessages = new ArrayList<>();
        }
        this.sentMessages.add(message);
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addProduct(Product p) {
        if (products == null) {
            products = new ArrayList<>();
        }
        products.add(p);
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname() + "[" + getUsername() + "]";
    }

}
