package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.dto.SprintBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintService;
import java.util.List;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SprintDetail {

    @Autowired
    SprintService sprintService;
    @Autowired
    SprintBacklogItemService backlogItemService;

    private SprintDTO sprint;

    public void setFinished() {
        Long productId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productBacklogItemId"));
        backlogItemService.setFinished(productId);
    }
    
    public Integer getTimeLeft() {
        return sprintService.daysLeft(sprint);
    }
    
    public String closeSprint(String outcome) {
        sprintService.closeSprint(sprint.getId());
        return outcome;
    }

    public String setSprintById(String outcome) {
        Long sprintId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sprintId"));
        this.sprint = sprintService.getAsDTO(sprintId);
        return outcome;
    }

    public List<SprintBacklogItemDTO> getItems() {
        return backlogItemService.getSprintBacklogItemsBySprint(sprint.getId());
    }

    public SprintDTO getSprint() {
        return sprint;
    }

}
