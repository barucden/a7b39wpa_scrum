package cz.cvut.fel.a7b39wpa.scrum.dto;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority;

public class ProductBacklogItemDTO extends AbstractBacklogItemDTO {

    private Long product;
    private Priority priority;

    public ProductBacklogItemDTO(Long id, String name, String description, AbstractBacklogItem.ItemStatus status, Long product, Priority priority) {
        super(id, name, description, status);
        this.product = product;
        this.priority = priority;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return getName();
    }
    
}
