package cz.cvut.fel.a7b39wpa.scrum.dao;

import cz.cvut.fel.a7b39wpa.scrum.bo.Message;
import org.springframework.stereotype.Component;

@Component
public class MessageDAO extends BaseDAO<Message, Long> {

    public MessageDAO() {
        super(Message.class);
    }
        

}
