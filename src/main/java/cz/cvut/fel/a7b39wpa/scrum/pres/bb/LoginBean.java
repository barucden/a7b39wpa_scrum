package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session")
public class LoginBean implements Serializable {

    @Autowired
    protected UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;

    protected String username;
    protected String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
        return "/login.xhtml?faces-redirect=true";
    }

    public String login() {
        Authentication request = new UsernamePasswordAuthenticationToken(username, password);
        try {
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
        } catch (org.springframework.security.core.AuthenticationException e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid credentials!", "The username/password combination is not valid.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return "login?faces-redirect=true";
        }
        return "www/index?faces-redirect=true";

    }
}
