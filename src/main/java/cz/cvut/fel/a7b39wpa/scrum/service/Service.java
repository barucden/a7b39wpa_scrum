package cz.cvut.fel.a7b39wpa.scrum.service;

import java.util.List;

public interface Service<T, ID> {

    T get(ID id);

    List<T> getAll();

    void persist(T instance);

    void remove(T instance);

    void removeById(ID id);

    void update(T instance);

}
