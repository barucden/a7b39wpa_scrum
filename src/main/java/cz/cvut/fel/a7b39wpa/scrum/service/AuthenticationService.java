package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.dao.UserDAO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

public class AuthenticationService extends AbstractUserDetailsAuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationService.class);

    private UserDAO userDAO;

    private TransactionTemplate transactionTemplate;

    public AuthenticationService() {
        this.setUserCache(new NullUserCache());
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails ud, UsernamePasswordAuthenticationToken upat) {
        // Do nothing
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken upat) throws org.springframework.security.core.AuthenticationException {
        return (UserDetails) transactionTemplate.execute(new TransactionCallback() {

            @Override
            public Object doInTransaction(TransactionStatus status) {
                try {
                    UserDetails ud = null;

                    cz.cvut.fel.a7b39wpa.scrum.bo.User u;
                    try {
                        u = userDAO.findByUsername(username);
                    } catch (IllegalArgumentException erdaex) {
                        throw new BadCredentialsException("User not found.");
                    }
                    String password = (String) upat.getCredentials();
                    if (u == null || !u.hasPassword(password)) {
                        AuthenticationException e = new BadCredentialsException("Invalid credentials.");
                        throw e;
                    } else {
                        List<GrantedAuthority> auths = new ArrayList<>();
                        auths.add(new SimpleGrantedAuthority("ROLE_USER"));
                        ud = new User(u.getUsername(), u.getPassword(), auths);
                    }
                    return ud;
                } catch (AuthenticationException e) {
                    status.setRollbackOnly();
                    throw e;
                } catch (Exception e) {
                    LOG.error("Error occured during retrieveUser call", e);
                    status.setRollbackOnly();
                    throw new RuntimeException(e);
                }
            }
        });
    }

}
