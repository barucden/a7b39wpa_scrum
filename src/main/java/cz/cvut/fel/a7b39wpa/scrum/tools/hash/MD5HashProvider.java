package cz.cvut.fel.a7b39wpa.scrum.tools.hash;

import java.math.BigInteger;
import java.security.MessageDigest;
import org.springframework.stereotype.Component;

@Component("md5HashProvider")
public class MD5HashProvider implements HashProvider {

    private static final String SALT = "j*)J':24A(*&@/asd.zdsf243][L2=4-P";

    @Override
    public String hash(String s) {
        s = s.concat(SALT);
        MessageDigest md;
        String result;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(s.getBytes(), 0, s.length());
            result = new BigInteger(1, md.digest()).toString();
        } catch (Exception ex) {
            throw new RuntimeException("[MD5] Hashing password has failed.");
        }

        return result;
    }

}
