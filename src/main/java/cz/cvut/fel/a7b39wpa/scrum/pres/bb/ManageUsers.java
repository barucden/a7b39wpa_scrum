package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.dto.UserDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request")
public class ManageUsers {

    @Autowired
    private UserService userService;
    
    public List<UserDTO> getUsers() {
        return userService.getAllAsDTO();
    }

    public String delete(Long id) {
        userService.removeById(id);
        return "manage-users?faces-redirect=true";
    }

}
