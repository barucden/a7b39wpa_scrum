package cz.cvut.fel.a7b39wpa.scrum.pres.valid;

import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductNameValidator implements Validator {

    @Autowired
    private ProductService productService;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String name = (String) value;
        Product p = productService.findByName(name);
        if (p != null) {
            throw new ValidatorException(new FacesMessage("The product name already exists.", "Choose a different name."));
        }
    }

}
