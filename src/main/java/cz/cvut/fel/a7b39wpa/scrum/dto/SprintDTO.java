package cz.cvut.fel.a7b39wpa.scrum.dto;

import java.util.Date;
import java.util.List;

public class SprintDTO extends AbstractDTO {

    private Date startDate;
    private Date endDate;
    private String name;
    private String description;
    private Long product;
    private Boolean closed;
    private List<Long> sprintBacklogItems;

    public SprintDTO(Long id, Date startDate, Date endDate, String name, String description, Long product, Boolean closed, List<Long> sprintBacklogItems) {
        super(id);
        this.startDate = startDate;
        this.endDate = endDate;
        this.name = name;
        this.description = description;
        this.product = product;
        this.closed = closed;
        this.sprintBacklogItems = sprintBacklogItems;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public List<Long> getSprintBacklogItems() {
        return sprintBacklogItems;
    }

    public void setSprintBacklogItems(List<Long> sprintBacklogItems) {
        this.sprintBacklogItems = sprintBacklogItems;
    }

}
