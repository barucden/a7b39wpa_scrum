package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.ProductDAO;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dao.ProductBacklogItemDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.UserDAO;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.trans.DTOTransformer;
import cz.cvut.fel.a7b39wpa.scrum.tools.trans.ProductBacklogItemDTOTransformer;
import cz.cvut.fel.a7b39wpa.scrum.tools.trans.ProductDTOTransformer;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService extends AbstractService<Product, Long> {

    @Autowired
    private ProductDAO primaryDAO;

    @Autowired
    private ProductBacklogItemDAO pbiDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    protected GenericDAO<Product, Long> getPrimaryDAO() {
        return primaryDAO;
    }

    @Transactional
    public Long addProduct(String name, Long ownerId, String description, List<Long> memberIds) {
        User owner = userDAO.loadById(ownerId);
        List<User> members = userDAO.getUsersByID(memberIds);
        Product p = new Product(name, owner, description, members);
        persist(p);
        for (User u : members) {
            u.addProduct(p);
        }
        return p.getId();
    }

    public List<Product> getProductsForUser(Long userId) {
        User u = userDAO.find(userId);
        return u.getProducts();
    }

    @Transactional(readOnly = true)
    @PreAuthorize("@permissionService.hasPermissionToSeeProduct(#productId)")
    public List<ProductBacklogItemDTO> getRemainingBacklogItems(Long productId) {
        Product product = primaryDAO.find(productId);
        List<ProductBacklogItem> pbis = pbiDAO.getRemainingBacklogItemsByProduct(product.getId());
        List<ProductBacklogItemDTO> pbiDTOs = new ArrayList<>(pbis.size());
        DTOTransformer<ProductBacklogItem, ProductBacklogItemDTO> transformer = new ProductBacklogItemDTOTransformer();
        for (ProductBacklogItem pbi : pbis) {
            pbiDTOs.add(transformer.getDTO(pbi));
        }
        return pbiDTOs;
    }

    @PreAuthorize("@permissionService.hasPermissionToSeeProduct(#id)")
    public ProductDTO getAsDTO(Long id) {
        return new ProductDTOTransformer().getDTO(get(id));
    }

    //neni preauthorized - pouzivano pouze ve validatoru, kde je nutne mit pristup k ostatnim produktum
    public Product findByName(String name) {
        return primaryDAO.getByName(name);
    }

}
