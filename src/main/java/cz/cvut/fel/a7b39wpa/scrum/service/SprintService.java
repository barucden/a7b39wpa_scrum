package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.Sprint;
import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.ProductBacklogItemDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.ProductDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.SprintBacklogItemDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.SprintDAO;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.trans.SprintDTOTransformer;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SprintService extends AbstractService<Sprint, Long> {

    private static final int MILISECONDS_IN_A_DAY = 86400000;

    @Autowired
    private SprintDAO primaryDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private SprintBacklogItemDAO sprintBacklogItemDAO;

    @Autowired
    private ProductBacklogItemDAO productBacklogItemDAO;

    @Override
    protected GenericDAO<Sprint, Long> getPrimaryDAO() {
        return primaryDAO;
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToSeeProduct(#productId)")
    public Long addSprint(Date startDate, Date endDate, String name, String description, Long productId) {
        if (startDate.after(endDate)) {
            throw new IllegalArgumentException("The start date must be before the end date.");
        }
        Product product = productDAO.find(productId);
        Sprint sprint = new Sprint(startDate, endDate, name, description, product, false);
        persist(sprint);
        return sprint.getId();
    }

    @Transactional(readOnly = true)
    @PreAuthorize("@permissionService.hasPermissionToSeeProduct(#productId)")
    public List<SprintDTO> getSprintsByProduct(Long productId) {
        return new SprintDTOTransformer().getDTOList(primaryDAO.getSprintsByProduct(productId));
    }

    public SprintDTO getAsDTO(Long id) {
        Sprint sprint = primaryDAO.find(id);
        return new SprintDTOTransformer().getDTO(sprint);
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToSeeSprint(#id)")
    public void closeSprint(Long id) {
        primaryDAO.closeSprint(id);
        List<SprintBacklogItem> sbis = sprintBacklogItemDAO.getSprintBacklogItemsBySprint(id);
        for (SprintBacklogItem i : sbis) {
            if (i.getStatus().equals(AbstractBacklogItem.ItemStatus.FINISHED)) {
                ProductBacklogItem pbi = i.getProductBacklogItem();
                productBacklogItemDAO.setState(pbi.getId(), AbstractBacklogItem.ItemStatus.FINISHED);
            }
        }
    }

    public int daysLeft(SprintDTO s) {
        long now = new Date().getTime();
        long diff = s.getEndDate().getTime() - now;
        int result = (int) (diff / MILISECONDS_IN_A_DAY);
        return result < 0 ? 0 : result;
    }

}
