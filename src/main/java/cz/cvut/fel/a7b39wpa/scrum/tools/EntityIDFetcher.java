package cz.cvut.fel.a7b39wpa.scrum.tools;

import java.util.ArrayList;
import java.util.List;
import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractEntity;

public class EntityIDFetcher {

    public static List<Long> getIDs(List<? extends AbstractEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<Long> result = new ArrayList<>(entities.size());
        for (AbstractEntity entity : entities) {
            result.add(entity.getId());
        }
        return result;
    }
}
