package cz.cvut.fel.a7b39wpa.scrum.dao;

import cz.cvut.fel.a7b39wpa.scrum.bo.Sprint;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class SprintDAO extends BaseDAO<Sprint, Long> {

    public SprintDAO() {
        super(Sprint.class);
    }

    public List<Sprint> getSprintsByProduct(Long id) {
        return entityManager.createQuery("SELECT s FROM " + type.getName() + " s WHERE s.product.id = ?1", type).setParameter(1, id).getResultList();
    }
    
    public void closeSprint(Long id) {
        Sprint sprint = find(id);
        sprint.setClosed(true);
        update(sprint);
    }
    
}
