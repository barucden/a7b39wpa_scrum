package cz.cvut.fel.a7b39wpa.scrum.dto;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus;

public class AbstractBacklogItemDTO extends AbstractDTO {

    private String name;
    private String description;
    private ItemStatus status;

    public AbstractBacklogItemDTO(Long id, String name, String description, ItemStatus status) {
        super(id);
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

}
