package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.EntityIDFetcher;
import java.util.ArrayList;
import java.util.List;

public class SprintBacklogDTOTransformer implements DTOTransformer<SprintBacklogItem, SprintBacklogItemDTO>, DTOMultiTransformer<SprintBacklogItem, SprintBacklogItemDTO> {

    @Override
    public SprintBacklogItemDTO getDTO(SprintBacklogItem t) {
        return new SprintBacklogItemDTO(t.getId(), t.getName(), t.getDescription(), t.getStatus(), t.getSprint().getId(), t.getProductBacklogItem().getId(), EntityIDFetcher.getIDs(t.getTasks()));
    }

    @Override
    public List<SprintBacklogItemDTO> getDTOList(List<SprintBacklogItem> l) {
        List<SprintBacklogItemDTO> result = new ArrayList<>(l.size());
        for (SprintBacklogItem i : l) {
            if (i != null) {
                result.add(getDTO(i));
            }
        }
        return result;
    }

}
