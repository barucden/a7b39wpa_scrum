package cz.cvut.fel.a7b39wpa.scrum.dto;

public abstract class AbstractDTO {

    protected Long id;

    public AbstractDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
