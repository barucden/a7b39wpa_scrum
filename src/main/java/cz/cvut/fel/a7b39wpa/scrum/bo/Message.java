package cz.cvut.fel.a7b39wpa.scrum.bo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Message extends AbstractEntity implements Serializable {

    public enum MessageType {

        IMPORTANT,
        WARNING,
        REQUEST,
        INVITATION;
    };

    @ManyToOne
    private User sender;

    @ManyToOne
    private User receiver;

    @Enumerated(value = EnumType.STRING)
    private MessageType type;

    private String text;

    private String subject;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timeSent;

    private Boolean isRead;

    protected Message() {
    }

    public Message(String text, String subject, User sender, User receiver, Date timeSent, MessageType type, Boolean isRead) {
        this.text = text;
        this.subject = subject;
        this.sender = sender;
        this.receiver = receiver;
        this.timeSent = timeSent;
        this.type = type;
        this.isRead = isRead;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Date getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "From: " + sender.getName() + " " + sender.getSurname() + "\nTo: " + receiver.getName() + " " + receiver.getSurname() + "\nDate: " + timeSent;
    }
}
