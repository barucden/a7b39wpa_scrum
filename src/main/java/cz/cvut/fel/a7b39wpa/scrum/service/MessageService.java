package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.bo.Message;
import cz.cvut.fel.a7b39wpa.scrum.bo.Message.MessageType;
import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.MessageDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.UserDAO;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MessageService extends AbstractService<Message, Long> {

    @Autowired
    private MessageDAO primaryDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    protected GenericDAO<Message, Long> getPrimaryDAO() {
        return primaryDAO;
    }

    public MessageType[] getMessageTypes() {
        return Message.MessageType.values();
    }

    @Transactional
    public void sendMessage(Long senderId, Long receiverId, String subject, String text, MessageType messageType) {
        User sender = userDAO.loadById(senderId);
        User receiver = userDAO.loadById(receiverId);
        Message m = new Message(text, subject, sender, receiver, new Date(), messageType, false);
        sender.addSentMessage(m);
        receiver.addReceivedMessage(m);
        primaryDAO.persist(m);
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToSeeUsersMessages(#userId)")
    public List<Message> getReceivedMessages(Long userId) {
        List<Message> messages = userDAO.getReceivedMesseges(userId);;
        for (Message m : messages) {
            if (!m.getIsRead()) {
                m.setIsRead(true);
                primaryDAO.update(m);
            }
        }
        return messages;
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToSeeUsersMessages(#userId)")
    public List<Message> getSentMessages(Long userId) {
        return userDAO.getSentMesseges(userId);
    }

    @PreAuthorize("@permissionService.hasPermissionToSeeUsersMessages(#userId)")
    public Integer getUnreadMessagesCount(Long userId) {
        User user = userDAO.find(userId);
        int count = 0;
        for (Message m : user.getReceivedMessages()) {
            if (!m.getIsRead()) {
                count++;
            }
        }
        return count;
    }
}
