package cz.cvut.fel.a7b39wpa.scrum.dto;

import java.util.Date;

public class TaskDTO extends AbstractDTO {

    private Long sprintBacklogItem;
    private String name;
    private String description;
    private Long author;

    public TaskDTO(Long id, Long sprintBacklogItem, String name, String description, Long author) {
        super(id);
        this.sprintBacklogItem = sprintBacklogItem;
        this.name = name;
        this.description = description;
        this.author = author;
    }

    public Long getSprintBacklogItem() {
        return sprintBacklogItem;
    }

    public void setSprintBacklogItem(Long sprintBacklogItem) {
        this.sprintBacklogItem = sprintBacklogItem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

}
