package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.Message;
import cz.cvut.fel.a7b39wpa.scrum.dto.UserDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.MessageService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request")
public class ReceivedMessages {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    public List<Message> getMessages() {
        UserDTO currentUser = userService.getCurrentUser();
        return messageService.getReceivedMessages(currentUser.getId());
    }

}
