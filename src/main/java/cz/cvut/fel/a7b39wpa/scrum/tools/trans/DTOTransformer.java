package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractEntity;
import cz.cvut.fel.a7b39wpa.scrum.dto.AbstractDTO;

public interface DTOTransformer<T1 extends AbstractEntity, T2 extends AbstractDTO> {

    public T2 getDTO(T1 t);

}
