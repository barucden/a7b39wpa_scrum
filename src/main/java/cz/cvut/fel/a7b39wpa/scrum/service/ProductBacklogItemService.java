package cz.cvut.fel.a7b39wpa.scrum.service;

import static cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus;
import cz.cvut.fel.a7b39wpa.scrum.bo.Product;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority;
import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.ProductBacklogItemDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.ProductDAO;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.trans.ProductBacklogItemDTOTransformer;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductBacklogItemService extends AbstractService<ProductBacklogItem, Long> {

    @Autowired
    private ProductBacklogItemDAO primaryDAO;

    @Autowired
    private ProductDAO productDAO;

    @Override
    protected GenericDAO<ProductBacklogItem, Long> getPrimaryDAO() {
        return primaryDAO;
    }

    @Transactional(readOnly = true)
    @PreAuthorize("@permissionService.hasPermissionToSeeProduct(#productId)")
    public List<ProductBacklogItemDTO> getProductBacklogItemsByProduct(Long productId) {
        Product p = productDAO.loadById(productId);
        return new ProductBacklogItemDTOTransformer().getDTOList(p.getProductBacklogItems());
    }

    @Transactional(readOnly = true)
    public ProductBacklogItemDTO getAsDto(Long id) {
        return new ProductBacklogItemDTOTransformer().getDTO(this.get(id));
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToAddPbiToProduct(#productId)")
    public Long addProductBacklogItem(Long productId, String name, String description, Priority priority) {
        Product product = productDAO.loadById(productId);
        ProductBacklogItem pbi = new ProductBacklogItem(product, name, description, priority, ItemStatus.TODO);
        productDAO.update(product);
        persist(pbi);
        product.addProductBacklogItem(pbi);
        return pbi.getId();
    }

}
