package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.Task;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.TaskService;
import java.util.List;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SprintBacklogItemDetail {

    @Autowired
    SprintBacklogItemService sprintBacklogItemService;
    @Autowired
    TaskService taskService;

    private SprintBacklogItemDTO sbi;

    public String setSprintBacklogItemById(String outcome) {
        Long sbiId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sprintBacklogItemId"));
        sbi = sprintBacklogItemService.getAsDTO(sbiId);
        return outcome;
    }

    public List<Task> getTasks() {
        return taskService.getTasksBySprintBacklogItem(sbi.getId());
    }

    public SprintBacklogItemDTO getSbi() {
        return sbi;
    }
    
    

}
