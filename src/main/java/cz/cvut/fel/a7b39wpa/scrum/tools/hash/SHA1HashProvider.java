package cz.cvut.fel.a7b39wpa.scrum.tools.hash;

import java.math.BigInteger;
import java.security.MessageDigest;
import org.springframework.stereotype.Component;

@Component("sha1HashProvider")
public class SHA1HashProvider implements HashProvider {

    private static final String SALT = "@#f44A]2!$#FA''@RTA";

    @Override
    public String hash(String s) {
        s = s.concat(SALT);
        MessageDigest md;
        String result;
        try {
            md = MessageDigest.getInstance("SHA-1");
            md.update(s.getBytes(), 0, s.length());
            result = new BigInteger(1, md.digest()).toString();
        } catch (Exception ex) {
            throw new RuntimeException("[SHA1] Hashing password has failed.");
        }

        return result;
    }

}
