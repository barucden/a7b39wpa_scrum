package cz.cvut.fel.a7b39wpa.scrum.bo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"item_id", "name"})
)
@Entity
public class Task extends AbstractEntity implements Serializable {

    @ManyToOne
    @JoinColumn(nullable = false, name = "item_id")
    private SprintBacklogItem item;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @JoinColumn(nullable = false)
    @ManyToOne
    private User author;

    protected Task() {
    }

    public Task(SprintBacklogItem item, String name, String description, User author) {
        this.item = item;
        this.name = name;
        this.description = description;
        this.author = author;
    }

    public SprintBacklogItem getItem() {
        return item;
    }

    public void setItem(SprintBacklogItem item) {
        this.item = item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

}
