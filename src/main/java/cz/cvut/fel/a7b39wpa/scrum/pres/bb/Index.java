package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.service.MessageService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request")
public class Index {                
    
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;
    
    private User user;

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public Integer getUnreadMessages() {
        return messageService.getUnreadMessagesCount(getUser().getId());
    }
    
    public User getUser() {
        if (user == null) {
            user = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return user;
    }  
    
}
