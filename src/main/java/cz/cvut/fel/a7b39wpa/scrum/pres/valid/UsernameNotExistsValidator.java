package cz.cvut.fel.a7b39wpa.scrum.pres.valid;

import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsernameNotExistsValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
          String username = (String) value;
        User u = userService.getUserByUsername(username);
        if (u == null) {
            throw new ValidatorException(new FacesMessage("User with this username does not exist. ", "Choose a different receiver."));
        }        
    }

}
