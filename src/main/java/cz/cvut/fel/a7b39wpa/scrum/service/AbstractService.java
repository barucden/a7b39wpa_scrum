package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractService<T, ID> implements Service<T, ID> {

    protected abstract GenericDAO<T, ID> getPrimaryDAO();

    @Transactional(readOnly = true)
    @Override
    public T get(ID id) {
        return getPrimaryDAO().find(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<T> getAll() {
        return getPrimaryDAO().findAll();
    }

    @Transactional
    @Override
    public void persist(T instance) {
        getPrimaryDAO().persist(instance);
    }

    @Transactional
    @Override
    public void remove(T instance) {
        getPrimaryDAO().remove(instance);
    }

    @Transactional
    @Override
    public void removeById(ID id) {
        T toRemove = getPrimaryDAO().loadById(id);
        if (toRemove != null) {
            getPrimaryDAO().remove(toRemove);
        }
    }

    @Transactional
    @Override
    public void update(T instance) {
        getPrimaryDAO().update(instance);
    }

}
