package cz.cvut.fel.a7b39wpa.scrum.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class BaseDAO<T, ID> implements GenericDAO<T, ID> {

    protected final Class<T> type;

    @PersistenceContext
    protected EntityManager entityManager;

    public BaseDAO(Class<T> type) {
        this.type = type;
    }

    @Override
    public List<T> findAll() {
        return entityManager.createQuery("SELECT e FROM " + type.getName() + " e", type).getResultList();
    }

    @Override
    public T find(ID id) {
        return entityManager.find(type, id);
    }

    @Override
    public Long getCount() {
        return entityManager.createQuery("SELECT count(e) FROM " + type.getName() + " e", Long.class).getSingleResult();
    }

    @Override
    public void remove(T instance) {
        entityManager.remove(instance);
    }

    @Override
    public void persist(T instance) {
        entityManager.persist(instance);
    }

    @Override
    public void update(T instance) {
        entityManager.merge(instance);
    }

    @Override
    public T loadById(ID id) {
        return entityManager.getReference(type, id);
    }

}
