package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.dto.SprintBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.TaskService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddNewTask {

    @Autowired
    SprintBacklogItemService backlogItemService;
    @Autowired
    TaskService taskService;
    @Autowired
    private UserService userService;

    private SprintBacklogItemDTO sbi;

    private String name;
    private String description;

    public String setSprintBacklogItemById(String outcome) {
        Long productId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sprintBacklogItemId"));
        sbi = backlogItemService.getAsDTO(productId);
        return outcome;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SprintBacklogItemDTO getSbi() {
        return sbi;
    }

    public String submit() {
        taskService.addTask(sbi.getId(), name, description, userService.getCurrentUser().getId());
        FacesMessage msg = new FacesMessage("Added.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        name = null;
        description = null;
        return "sprintDetail?facesredirect=true";
    }
}
