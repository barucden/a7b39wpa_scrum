package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem.Priority;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session")
public class AddNewProductBacklogItem {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductBacklogItemService productBacklogItemService;

    private ProductDTO product;

    private String name;
    private String description;
    private Priority priority;

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Priority[] getPriorities() {
        return Priority.values();
    }

    public String submit(String message) {
        productBacklogItemService.addProductBacklogItem(product.getId(), name, description, priority);
        FacesMessage msg = new FacesMessage(name, message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "productDetail";
    }

    public String cancel() {
        return "productDetail";
    }

    public String setProductById(String outcome) {
        Long productId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productId"));
        this.product = productService.getAsDTO(productId);
        name = null;
        description = null;
        priority = null;
        return outcome;
    }

}
