package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.bo.User;
import cz.cvut.fel.a7b39wpa.scrum.dto.UserDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import cz.cvut.fel.a7b39wpa.scrum.service.UserService;
import cz.cvut.fel.a7b39wpa.scrum.tools.EntityIDFetcher;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session")
public class CreateProductWizard {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    String productName;
    String productDescription;

    TeamPickList pickList;

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public TeamPickList getPickList() {
        if (pickList == null) {
            pickList = new TeamPickList(userService.getAll());
        }
        return pickList;
    }

    public void setPickList(TeamPickList pickList) {
        this.pickList = pickList;
    }

    public UserDTO getProductOwner() {
        return userService.getCurrentUser();
    }

    public String submit(String message) {
        UserDTO u = getProductOwner();
        if (u == null) {
            throw new RuntimeException("Product owner does not exist.");
        } else {
            List<User> members = pickList.getUsers().getTarget();
            List<Long> memberIds = EntityIDFetcher.getIDs(members);
            productService.addProduct(productName, u.getId(), productDescription, memberIds);
        }
        RequestContext.getCurrentInstance().reset("wizardform");
        RequestContext.getCurrentInstance().update("wizardform");
        productName = "";
        productDescription = "";
        pickList = null;       
        FacesMessage msg = new FacesMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "browseProducts";
    }
}
