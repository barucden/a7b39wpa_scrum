package cz.cvut.fel.a7b39wpa.scrum.service;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem.ItemStatus;
import cz.cvut.fel.a7b39wpa.scrum.bo.ProductBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.Sprint;
import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.dao.GenericDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.ProductBacklogItemDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.SprintBacklogItemDAO;
import cz.cvut.fel.a7b39wpa.scrum.dao.SprintDAO;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.trans.SprintBacklogDTOTransformer;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SprintBacklogItemService extends AbstractService<SprintBacklogItem, Long> {

    @Autowired
    private SprintBacklogItemDAO primaryDAO;

    @Autowired
    private SprintDAO sprintDAO;

    @Autowired
    private ProductBacklogItemDAO productBacklogItemDAO;

    @Override
    protected GenericDAO<SprintBacklogItem, Long> getPrimaryDAO() {
        return primaryDAO;
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToSeeSprint(#sprintId)")
    public Long addSprintBacklogItem(Long sprintId, Long productBacklogItemId, String name, String description, ItemStatus status) {
        Sprint sprint = sprintDAO.find(sprintId);
        ProductBacklogItem productBacklogItem = productBacklogItemDAO.find(productBacklogItemId);
        productBacklogItem.setStatus(ItemStatus.IN_PROGRESS);
        SprintBacklogItem sbi = new SprintBacklogItem(sprint, productBacklogItem, name, description, status);
        persist(sbi);
        productBacklogItemDAO.update(productBacklogItem);
        sprint.addSprintBacklogItem(sbi);
        return sbi.getId();
    }

    @Transactional(readOnly = true)
    @PreAuthorize("@permissionService.hasPermissionToSeeSprint(#sprintId)")
    public List<SprintBacklogItemDTO> getSprintBacklogItemsBySprint(Long sprintId) {
        List<SprintBacklogItem> items = primaryDAO.getSprintBacklogItemsBySprint(sprintId);
        return new SprintBacklogDTOTransformer().getDTOList(items);
    }

    @PreAuthorize("@permissionService.hasPermissionToSeeSbi(#id)")
    public SprintBacklogItemDTO getAsDTO(Long id) {
        SprintBacklogItem item = get(id);
        return new SprintBacklogDTOTransformer().getDTO(item);
    }

    @Transactional
    @PreAuthorize("@permissionService.hasPermissionToSeeSbi(#itemId)")
    public void setFinished(Long itemId) {
        primaryDAO.setFinished(itemId);
    }

}
