package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.dto.ProductBacklogItemDTO;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.model.DualListModel;

public class ProductBacklogItemsPickList {

    private DualListModel<ProductBacklogItemDTO> items;

    public ProductBacklogItemsPickList(List<ProductBacklogItemDTO> items) {
        this.items = new DualListModel<>(items, new ArrayList<>());
    }

    public DualListModel<ProductBacklogItemDTO> getItems() {
        return items;
    }

    public void setItems(DualListModel<ProductBacklogItemDTO> items) {
        this.items = items;
    }

}
