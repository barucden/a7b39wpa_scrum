package cz.cvut.fel.a7b39wpa.scrum.pres.bb;

import cz.cvut.fel.a7b39wpa.scrum.dto.ProductBacklogItemDTO;
import cz.cvut.fel.a7b39wpa.scrum.dto.ProductDTO;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintDTO;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductBacklogItemService;
import cz.cvut.fel.a7b39wpa.scrum.service.ProductService;
import cz.cvut.fel.a7b39wpa.scrum.service.SprintService;
import java.util.List;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session")
public class ProductDetail {

    private ProductDTO product;

    @Autowired
    private ProductService productService;
    @Autowired
    private SprintService sprintService;

    @Autowired
    private ProductBacklogItemService productBacklogItemService;

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public List<ProductBacklogItemDTO> getItems() {
        return productBacklogItemService.getProductBacklogItemsByProduct(product.getId());
    }

    public String setProductById(String outcome) {
        Long productId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("productId"));
        this.product = productService.getAsDTO(productId);
        return outcome;
    }

    public List<SprintDTO> getSprints() {
        return sprintService.getSprintsByProduct(product.getId());
    }

}
