package cz.cvut.fel.a7b39wpa.scrum.tools.trans;

import cz.cvut.fel.a7b39wpa.scrum.bo.Sprint;
import cz.cvut.fel.a7b39wpa.scrum.dto.SprintDTO;
import cz.cvut.fel.a7b39wpa.scrum.tools.EntityIDFetcher;
import java.util.ArrayList;
import java.util.List;

public class SprintDTOTransformer implements DTOTransformer<Sprint, SprintDTO>, DTOMultiTransformer<Sprint, SprintDTO> {

    @Override
    public SprintDTO getDTO(Sprint t) {
        return new SprintDTO(t.getId(), t.getStartDate(), t.getEndDate(), t.getName(), t.getDescription(), t.getProduct().getId(), t.getClosed(), EntityIDFetcher.getIDs(t.getSprintBacklogItems()));
    }

    @Override
    public List<SprintDTO> getDTOList(List<Sprint> l) {
        List<SprintDTO> result = new ArrayList<>(l.size());
        for (Sprint s : l) {
            if (s != null) {
                result.add(getDTO(s));
            }
        }
        return result;
    }

}
