package cz.cvut.fel.a7b39wpa.scrum.dao;

import cz.cvut.fel.a7b39wpa.scrum.bo.AbstractBacklogItem;
import cz.cvut.fel.a7b39wpa.scrum.bo.Sprint;
import cz.cvut.fel.a7b39wpa.scrum.bo.SprintBacklogItem;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SprintBacklogItemDAO extends BaseDAO<SprintBacklogItem, Long> {

    @Autowired
    SprintDAO sprintDAO;
    
    public SprintBacklogItemDAO() {
        super(SprintBacklogItem.class);
    }
    
    public List<SprintBacklogItem> getSprintBacklogItemsBySprint(Long sprintId) {
        Sprint sprint = sprintDAO.find(sprintId);
        return sprint.getSprintBacklogItems();
    }

    public void setFinished(Long itemId) {
        SprintBacklogItem i = find(itemId);
        i.setStatus(AbstractBacklogItem.ItemStatus.FINISHED);
        update(i);
    }
    
}
