package cz.cvut.fel.a7b39wpa.scrum.tools.hash;

public interface HashProvider {

    public String hash(String s);

}
